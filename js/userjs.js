$(document).ready(function(){
  $('.member-pc').append($('.hdmenu ul'));
     //輪播圖
        $('.mads-li').owlCarousel({
          loop:true,
          margin:0,
          nav:false,
          items:1,
          dots:true,
          animateOut: 'fadeOut',
          autoplay:true,
          autoplayHoverPause:false
        });   

        $('.thumb-large > div').owlCarousel({
            items:1,
            loop:true,
            nav: true,
            dots: false,
            smartSpeed: 450,
            autoplay:false
        });


    if ($(window).width() > 768) {      
      $('.btn-group li').hover(function(){
        $(this).children('.showarea').toggleClass('show');
      });
    }else if ($(window).width() < 767){
      $('.btn-group li a').click(function(){
        $(this).parent('li').children('.showarea').toggleClass('show');
      });

    }
    $('.btn-group a.top').click(function () {
      $("html, body").animate({
          scrollTop: 0
      }, 600);
      return false;
  });
    $('.module-ecptdetail .mpro-panel .mpro-price').before($('.detail'));
    //特效
        if ($(window).width() > 767) {

        //把控制項加入陣列       
        var fadein_index=[
                ".page-chara .col-sm-4",
                ".page-chara-link .col-sm-3",
                ".page-person .col-sm-5",
                ".page-person .col-sm-7",
                ".module-ptlist .mbox",
                ".page_mobileptdetail .feature-pic"
                ];
        var bounce_index=[
                ".page-scuess .col-sm-4",
        ]
        //滾動animation
        //當視窗高度到達物件時的動作
        function scrollanimation(a, b) {
            $(a).each(function() {
                // Check the location of each desired element //
                var objectBottom = $(this).offset().top + $(this).outerHeight();
                var windowBottom = $(window).scrollTop() + $(window).innerHeight() + 500;
                if (objectBottom < windowBottom) {
                    $(this).addClass(b);
                }
            });
        };

        //遇到class+animation
        $(window).on("load", function() {
              // fadein_index.forEach(function(e){
              //   $(e).css('opacity','0');
              // });       
              // bounce_index.forEach(function(e){
              //   $(e).css('opacity','0');
              // });       
            $(window).scroll(function() {
                fadein_index.forEach(function(e){
                        scrollanimation(e,'animated fadeIn');
                });
                bounce_index.forEach(function(e){
                        scrollanimation(e,'animated bounceIn');
                });

            });
        });
    }
}); 